package com.a4bee;

public class Frame {

    private int knockedInFirstRoll;
    private int knockedInSecondRoll;

    public Frame(int knockedInFirstRoll, int knockedInSecondRoll) {
        this.knockedInFirstRoll = knockedInFirstRoll;
        this.knockedInSecondRoll = knockedInSecondRoll;
    }

    public Frame(int knockedInFirstRoll) {
        this.knockedInFirstRoll = knockedInFirstRoll;
    }

    public static Frame strike() {
        return new Frame(10);
    }

    public static Frame spare(int knockedInFirstRoll) {
        return new Frame(knockedInFirstRoll, 10 - knockedInFirstRoll);
    }

    public int getKnockedInFirstRoll() {
        return knockedInFirstRoll;
    }

    public int getKnockedInSecondRoll() {
        return knockedInSecondRoll;
    }

    public int getFrameScore() {
        return knockedInFirstRoll + knockedInSecondRoll;
    }

    public boolean isStrike() {
        return knockedInFirstRoll == 10;
    }

    public boolean isSpare() {
        return knockedInFirstRoll + knockedInSecondRoll == 10;
    }
}
