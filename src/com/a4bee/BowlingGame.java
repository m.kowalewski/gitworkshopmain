package com.a4bee;

public class BowlingGame {

    private Frame[] frames;

    public BowlingGame(Frame[] frames) {
        this.frames = frames;
    }

    public int calculateScore() {
        int score = 0;
        for (int i = 0; i < 10; i++) {
            score += calculateFrameScore(i);
        }
        return score;
    }

    private int calculateFrameScore(int frameIndex) {
        if (this.frames[frameIndex].isStrike()) {
            return getStrikeFrameScore(frameIndex);
        } else if (this.frames[frameIndex].isSpare()) {
            return calculateSpareScore(frameIndex);
        } else {
            return getFrameScore(frameIndex);
        }
    }

    private int getFrameScore(int frameIndex) {
        return this.frames[frameIndex].getFrameScore();
    }

    private int getStrikeFrameScore(int frameIndex) {
        int strikeFrameScore = 0;
        strikeFrameScore += getFrameScore(frameIndex);
        strikeFrameScore += getFrameScore(frameIndex + 1);
        if (this.frames[frameIndex + 1].isStrike()) {
            strikeFrameScore += this.frames[frameIndex + 2].getKnockedInFirstRoll();
        }
        return strikeFrameScore;
    }

    private int calculateSpareScore(int frameIndex) {
        return getFrameScore(frameIndex) + this.frames[frameIndex + 1].getKnockedInFirstRoll();
    }

}
